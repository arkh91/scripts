#!/bin/bash
# A sample Bash script to set up discord.py environment

sudo apt install python3-pip nodejs -y
sudo pip3 install discord.py
sudo pip3 install python-dotenv

pip3 install discord.py python-dotenv
pip3 install mysql-connector
pip3 install radiojavanapi
python3 -m pip install beautifulsoup4
#pm2 start index.py --name=pythonbot --interpreter=python3
